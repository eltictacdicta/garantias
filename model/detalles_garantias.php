<?php
/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2014-2015  Francisco Javier Trujillo   javier.trujillo.jimenez@gmail.com
 * Copyright (C) 2014  Carlos Garcia Gomez         neorazorx@gmail.com
 * Copyright (C) 2015  Francesc Pineda Segarra     shawe.ewahs@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class detalles_garantias extends fs_model
{
   public $id;
   public $descripcion;
   public $ngarantias;
   public $fecha;
   
   public function __construct($s = FALSE)
   {
      parent::__construct('detalles_garantias', 'plugins/garantias/');
      
      if($s)
      {
         $this->id = intval($s['id']);
         $this->descripcion = $s['descripcion'];
         $this->ngarantias = intval($s['ngarantias']);
         $this->fecha = date('d-m-Y', strtotime($s['fecha']));
      }
      else
      {
         $this->id = NULL;
         $this->descripcion = '';
         $this->ngarantias = NULL;
         $this->fecha = date('d-m-Y');
      }
   }
   
   public function install()
   {
      return '';
   }
   
   
   
   public function get($id)
   {
      $sql = "SELECT id, descripcion, ngarantias, fecha FROM detalles_garantias WHERE id = ".$this->var2str($id).";";
      $data = $this->db->select($sql);
      
      if($data)
      {
         return new detalles_garantias($data[0]);
      }
      else
      {
         return FALSE;
      }
   }
   
   public function exists()
   {
      if( is_null($this->id) )
      {
         return FALSE;
      }
      else
      {
         return $this->db->select("SELECT * FROM detalles_garantias WHERE id = ".$this->var2str($this->ngarantias).";");
      }
   }
   
   public function test()
   {
      $this->descripcion = $this->no_html($this->descripcion);
      
      /// realmente no quería comprobar nada, simplemente eliminar el html de las variables
      return TRUE;
   }
   
   public function save()
   {
      if( $this->test() )
      {
         if( $this->exists() )
         {
            $sql = "UPDATE detalles_garantias SET descripcion = ".$this->var2str($this->descripcion).", "
               . "fecha = ".$this->var2str($this->fecha).", ngarantias = ".$this->var2str($this->ngarantias)." "
               . "WHERE id = ".$this->var2str($this->id).";";
            
            return $this->db->exec($sql);
         }
         else
         {
            $sql = "INSERT INTO detalles_garantias (descripcion,fecha,ngarantias) VALUES ("
                    .$this->var2str($this->descripcion).", ".$this->var2str($this->fecha).", ".$this->var2str($this->ngarantias).");";
            
            if( $this->db->exec($sql) )
            {
               $this->id = $this->db->lastval();
               return TRUE;
            }
            else
            {
               return FALSE;
            }
         }
      }
      else
      {
         return FALSE;
      }
   }
   
   public function delete()
   {
      
   }
   
   public function all()
   {
      $detalleslist = array();
      
      $sql = "SELECT dg.id, dg.descripcion,dg.ngarantias, dg.fecha "
         . "FROM registros_garantias rg, detalles_garantias dg "
         . "WHERE dg.ngarantias = rg.ngarantias ORDER BY fecha ASC, id ASC;";
      $data = $this->db->select($sql);
      
      if($data)
      {
         foreach($data as $d)
         {
            $detalleslist[] = new detalles_garantias($d);
         }
      }
      
      return $detalleslist;
   }
   
   public function all_from_garantias($garantias)
   {
      $detalleslist = array();
      
      $sql = "SELECT dg.id, dg.descripcion, dg.ngarantias, dg.fecha "
         . "FROM registros_garantias rg, detalles_garantias dg "
         . "WHERE dg.ngarantias = rg.ngarantias AND dg.ngarantias = $garantias ORDER BY fecha ASC, id ASC;";
      $data = $this->db->select($sql);
      
      if($data)
      {
         foreach($data as $d)
         {
            $detalleslist[] = new detalles_garantias($d);
         }
      }
      
      return $detalleslist;
   }
   
}
