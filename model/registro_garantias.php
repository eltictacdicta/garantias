<?php
/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2014-2015    Francisco Javier Trujillo   javier.trujillo.jimenez@gmail.com
 * Copyright (C) 2014-2016    Carlos Garcia Gomez         neorazorx@gmail.com
 * Copyright (C) 2015         Francesc Pineda Segarra     shawe.ewahs@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class registro_garantias extends fs_model
{
   //-----Datos usados-------------------
   public $ngarantias;
   public $prioridad;
   public $fentrada;
   public $modelo;
   public $estado;
   public $averia;
   public $accesorios;
   public $codcliente;
   public $fechfactventa;
   public $factventa;
   public $codproveedor;
   public $fechfactcompra;
   public $factcompra;
   public $numserie;
   public $numrma;
   public $observaciones;
   public $posicion;
   public $globalsql;
   //-------------------------------------
   
   
   public function __construct($s = FALSE)
   {
      parent::__construct('registros_garantias', 'plugins/garantias/');
      
      if($s)
      {
         $this->ngarantias = intval($s['ngarantias']);
         $this->prioridad = intval($s['prioridad']);
         $this->fentrada = date('d-m-Y', strtotime($s['fentrada']));
         $this->modelo = $s['modelo'];
         $this->codcliente = $s['codcliente'];
         $this->estado = intval($s['estado']);
         $this->prioridad = intval($s['prioridad']);
         $this->averia = $s['averia'];
         $this->accesorios = $s['accesorios'];
         $this->observaciones = $s['observaciones'];
         $this->posicion = $s['posicion'];
         $this->factventa= $s['factventa'];
         $this->fechfactventa = NULL;
         if( isset($s['fechfactventa']) )
         {
             if($s['fechfactventa']!='')
                $this->fechfactventa = date('d-m-Y', strtotime($s['fechfactventa']));
         }
            
         $this->factcompra= $s['factcompra'];
         $this->fechfactcompra = NULL;
         if( isset($s['fechfactcompra']) )
         {
             if($s['fechfactcompra']!='')
                $this->fechfactcompra = date('d-m-Y', strtotime($s['fechfactcompra']));
         }
         
         if($s['codproveedor']!='')
         {
            $this->codproveedor= $s['codproveedor'];
         }
         $this->numserie= $s['numserie'];
         $this->numrma= $s['numrma'];
      }
      else
      {
         $this->ngarantias = NULL;
         $this->prioridad = 3;
         $this->fentrada = date('d-m-Y');
         $this->modelo = '';
         $this->codcliente = NULL;
         $this->estado = 1;
         $this->averia = '';
         $this->accesorios = '';
         $this->observaciones = '';
         $this->posicion = '';
         $this->factcompra = '';
         $this->fechfactventa = NULL;
         $this->factventa = '';
         $this->fechfactcompra = NULL;
         $this->codproveedor= NULL;
         $this->numserie= '';
         $this->numrma= '';
         
      }
   }
   
   public function install()
   {
      return '';
   }
   
   public function estados()
   {
      $estados = array(
          1 => 'Garantía por empezar',
          2 => 'Pendiente de nª de RMA',
          3 => 'Preparado paquete para enviar',
          4 => 'Paqueta enviado',
          5 => 'Paquete recivido',
          6 => 'Analisis realizado',
          7 => 'Garantia terminada'
      );
      
      return $estados;
   }
   
    public function prioridad()
   {
      $prioridad = array(
          1 => 'Urgente',
          2 => 'Prioridad alta',
          3 => 'Prioridad media',
          4 => 'Prioridad baja',
      );
      
      return $prioridad;
   }
   
   
   public function nombre_estado()
   {
      $estados = $this->estados();
      return $estados[$this->estado];
   }
   
   public function nombre_estado_param($estado)
   {
      $estados = $this->estados();
      return $estados[$estado];
   }
   
   
   public function nombre_proveedor()
   {
      $proveedor='No asignado';
      
      if(!is_null($this->codproveedor))//si no es nulo
      {
         $sql = "SELECT nombre FROM proveedores WHERE codproveedor = ".$this->var2str($this->codproveedor);
         $result = $this->db->select($sql);
         if($result)
         {
            $proveedor = $result[0]['nombre'];
         }
      }
      
      return $proveedor;
   }
   
   
   public function nombre_cliente()
   {
      $cliente='No asignado';
      
      if(!is_null($this->codcliente))//si no es nulo
      {
         $sql = "SELECT nombre FROM clientes WHERE codcliente = ".$this->var2str($this->codcliente);
         $result = $this->db->select($sql);
         if($result)
         {
            $cliente = $result[0]['nombre'];
         }
      }
      
      return $cliente;
   }
   
   
   
   public function nombre_prioridad()
   {
      $prioridades = $this->prioridad();
      return $prioridades[$this->prioridad];
   }
   
   
   public function url()
   {
      if( is_null($this->ngarantias) )
      {
         return 'index.php?page=listado_garantias';
      }
      else
      {
         return 'index.php?page=garantia&id='.$this->ngarantias;
      }
   }
   
   public function cliente_url()
   {
      if( is_null($this->codcliente) )
      {
         return "index.php?page=ventas_clientes";
      }
      else
      {
         return "index.php?page=ventas_cliente&cod=".$this->codcliente;
      }
   }
   
   public function get($id)
   {
      $sql = "SELECT ngarantias, prioridad, fentrada, modelo, codcliente, "
         . " estado, averia, accesorios, observaciones, posicion, "
         . " factventa, fechfactventa, codproveedor, factcompra, fechfactcompra, numserie, observaciones, numrma"
         . " FROM registros_garantias  "
         . "WHERE ngarantias = ".$this->var2str($id).";";
      $data = $this->db->select($sql);
      
      if($data)
      {
         return new registro_garantias($data[0]);
      }
      else
      {
         return FALSE;
      }
   }
   
   public function exists()
   {
      if( is_null($this->ngarantias) )
      {
         return FALSE;
      }
      else
      {
         return $this->db->select("SELECT * FROM registros_garantias WHERE ngarantias = ".$this->var2str($this->ngarantias).";");
      }
   }
   
   public function test()
   {
      $this->modelo = $this->no_html($this->modelo);
      $this->averia = $this->no_html($this->averia);
      $this->accesorios = $this->no_html($this->accesorios);
      $this->observaciones = $this->no_html($this->observaciones);
      $this->posicion = $this->no_html($this->posicion);
      //pendiente de añadir todos los campos
      
      /// realmente no quería comprobar nada, simplemente eliminar el html de las variables
      return TRUE;
   }
   
   public function save()
   {
      if( $this->test() )
      {
         if( $this->exists() )
         {
            $sql = "UPDATE registros_garantias SET prioridad = ".$this->var2str($this->prioridad).",
               modelo = ".$this->var2str($this->modelo).", codcliente = ".$this->var2str($this->codcliente).",
               estado = ".$this->var2str($this->estado).", averia = ".$this->var2str($this->averia).",
               accesorios = ".$this->var2str($this->accesorios).", observaciones = ".$this->var2str($this->observaciones).",
               posicion = ".$this->var2str($this->posicion).", factventa = ".$this->var2str($this->factventa).","
               . "fechfactventa = ".$this->var2str($this->fechfactventa).", codproveedor = ".$this->var2str($this->codproveedor).", factcompra = ".$this->var2str($this->factcompra).","
                    . " fechfactcompra = ".$this->var2str($this->fechfactcompra).", "
                    . " numserie = ".$this->var2str($this->numserie).", numrma = ".$this->var2str($this->numrma)." WHERE ngarantias = ".$this->var2str($this->ngarantias).";";
            
            return $this->db->exec($sql);
         }
         else
         {
            $sql = "INSERT INTO registros_garantias (prioridad, fentrada, modelo, codcliente, estado, averia, accesorios, observaciones, posicion, factventa, "
                . "fechfactventa,codproveedor, factcompra, fechfactcompra,numserie,numrma) VALUES ("
               .$this->var2str($this->prioridad).", ".$this->var2str($this->fentrada).", "            
               .$this->var2str($this->modelo).",".$this->var2str($this->codcliente).", "
               .$this->var2str($this->estado).",".$this->var2str($this->averia).", "
               .$this->var2str($this->accesorios).",".$this->var2str($this->observaciones).",".$this->var2str($this->posicion).", "
               .$this->var2str($this->factventa).",".$this->var2str($this->fechfactventa).", "
               .$this->var2str($this->codproveedor).",".$this->var2str($this->factcompra).",".$this->var2str($this->fechfactcompra).","
               .$this->var2str($this->numserie).",".$this->var2str($this->numrma).");";
            
            if( $this->db->exec($sql) )
            {
               $this->ngarantias = $this->db->lastval();
               return TRUE;
            }
            else
            {
               return FALSE;
            }
         }
      }
      else
      {
         return FALSE;
      }
   }
   
   public function delete()
   {
      return $this->db->exec("DELETE FROM registros_garantias WHERE ngarantias = ".$this->var2str($this->ngarantias).";");
   }
   
   public function all()
   {
      $garantiaslist = array();
      
      $sql = "SELECT ngarantias, prioridad, fentrada, modelo, codcliente, "
         ." estado, averia, accesorios, observaciones, posicion, factventa, fechfactventa"
         . ", codproveedor, factcompra,fechfactcompra, numserie, numrma "
         . "FROM registros_garantias";
      $data = $this->db->select($sql);
      
      if($data)
      {
         foreach($data as $d)
         {
            $garantiaslist[] = new registro_garantias($d);
         }
      }
      
      return $garantiaslist;
   }
   
   
   
   //este es el del SAT todavia no esta configurado
   public function search($buscar='', $estado='activos', $proveedor='todos')
   {
      $garantiaslist = array();
      
      $sql = "SELECT ngarantias, prioridad, fentrada, modelo, codcliente, "
         ." estado, averia, accesorios, observaciones, posicion, factventa, fechfactventa"
         . ", codproveedor, factcompra,fechfactcompra, numserie, numrma "
         . "FROM registros_garantias WHERE ((lower(modelo) LIKE lower('%".$buscar."%')) OR (observaciones LIKE '%".$buscar."%')
            OR (lower(numserie) LIKE lower('%".$buscar."%')))";
      
      
      if($proveedor != "todos")
      {
         $sql .= " AND codproveedor = ".$proveedor;
      }
      
      if($estado != "todos" AND $estado != "activos")
      {
         $sql .= " AND estado = ".$estado;
      }
      else 
      {
          if($estado == "activos")
          {
              $sql .= " AND estado != 7";
          }
          
          //si no entra en ninguno de los 2 if anteriores muestra todos los estados.
      }
      
      if($buscar != '')
      {
         
      }
      
      
      $sql.= " ORDER BY ngarantias ASC ";
      $this->globalsql=$sql;
      $data = $this->db->select($sql.";");
      if($data)
      {
         foreach($data as $d)
         {
            $garantiaslist[] = new registro_garantias($d);
         }
      }
      
      return $garantiaslist;
   }
   // me hara falta para cuando implemente los detalles
   public function num_detalles()
   {
      $sql = "SELECT count(*) as num FROM detalles_garantias WHERE ngarantias = ".$this->var2str($this->ngarantias).";";
      $result = $this->db->select($sql);
      
      return $this->num_detalles = intval($result[0]['num']);
   }
}
