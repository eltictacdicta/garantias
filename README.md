<h1>Plugin de "Gestión de garantías"</h1>

<strong>Ubicación del plugin:</strong> facturascripts/plugins/garantias

<br>

<strong>Descripción</strong>

Gestión de RMA para una tienda de informática o comercios similares.

<br>

<strong>Características:</strong>

<ul>
   <li>Por determinar</li>
</ul>

<br>

<strong>Características por implementar:</strong>

<ul>
    <li>Implementar el buscador de los listados.</li>
    <li>Implementar buscador clientes.</li>
   <li>Poder enlazar a traves de un buscador con las facturas de compra y venta.
    Pero sin perder la posibilidad de cargarlo a mano</li>
    <li>Poder adjuntar documento de venta y documento de compra si es necesario.</li>
    <li>Imprimir listado y etiquetas para cada producto.</li>
    <li>Poder enlazar con un SAT y carge los datos.</li>
</ul>

<br>

<strong>Sugerencias en consideración:</strong>

<ul>
   <li>Por rellenar</li>
</ul>

<br>

<strong>Apoya el plugin:</strong>

Puedes apoyar el desarrollo del plugin sugiriendo y enviando cambios. Puedes 
contactar conmigo en javier.trujillo.jimenez@gmail.com