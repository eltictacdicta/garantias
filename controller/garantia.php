<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2014  Francisco Javier Trujillo   javier.trujillo.jimenez@gmail.com
 * Copyright (C) 2014  Carlos Garcia Gomez         neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_model('agente.php');
require_model('proveedor.php');
require_model('cliente.php');
require_model('detalles_garantias.php');
require_model('registro_garantias.php');

//contructor para los listados
class garantia extends fs_controller
{
   public $agente;
   public $cliente;
   public $proveedor;
   public $registro_garantias;
   public $detalles_garantias;
   public $busqueda;
   public $resultado;
   public $estado;
   public $num_detalles;
   public $pais;
   public $urlplugin="plugins/garantias/";
   public function __construct() {
    parent::__construct(__CLASS__, 'Garantias', 'ventas', FALSE, FALSE);
      /// cualquier cosa que pongas aquí se ejecutará DESPUÉS de process()
   }

   /**
    * esta función se ejecuta si el usuario ha hecho login,
    * a efectos prácticos, este es el constructor
    */
   protected function private_core()
   {
        $this->show_fs_toolbar = FALSE;
        $this->agente = FALSE;
        $this->cliente = new cliente();
        $this->proveedor = new proveedor();
        $this->registro_garantias = new registro_garantias();
        $this->detalles_garantias = new detalles_garantias();
        $modelo="";
        $opcion="";
        if(isset($_POST['modelo']))//primero compruebo que he recibido el post para agregarselo a una variable
        {
            $modelo=$_POST['modelo'];
        } 
        if(isset($_GET['opcion']))//primero compruebo que he recibido el post para agregarselo a una variable
        {
            $opcion=$_GET['opcion'];
        } 

         if (isset($_GET['id']))
         {
             if($modelo!="")
             {
                $this->edita_garantias($_GET['id']);
                $this->resultado = $this->registro_garantias->get($_GET['id']);
             }
            else 
            {
                $this->resultado = $this->registro_garantias->get($_GET['id']);
            }
            
            $this->template = "editagarantia";
         }
         elseif ($modelo!="")//se se pasa algo en modelo se agrega cliente
         {
             
                 $id=$this->agrega_garantias();
                 $this->resultado = $this->registro_garantias->get($id);
                 $this->template = "editagarantia";
         }
         else 
         {
             $this->template = "agregagarantias";
         }
   }
   
   public function listar_estados()
   {
      $estados = array();

      /**
       * En registro_garantias::estados() nos devuelve un array con todos los estados,
       * pero como queremos también el id, pues hay que hacer este bucle para sacarlos.
       */
      foreach ($this->registro_garantias->estados() as $i => $value)
         $estados[] = array('id_estado' => $i, 'nombre_estado' => $value);

      return $estados;
   }

   public function listar_prioridad()
   {
      $prioridad = array();

      /**
       * En registro_garantias::prioridad() nos devuelve un array con todos los prioridades,
       * pero como queremos también el id, pues hay que hacer este bucle para sacarlos.
       */
      foreach ($this->registro_garantias->prioridad() as $i => $value)
         $prioridad[] = array('id_prioridad' => $i, 'nombre_prioridad' => $value);

      return $prioridad;
   }

   /* listar el dealle de garantias */
   public function listar_garantias_detalle()
   {
      return $this->detalles_garantias->all_from_garantias($_GET['id']);
   }

   public function agrega_detalle()
   {
      $detalle = new detalles_garantias();
      $detalle->descripcion = $_POST['detalle'];
      $detalle->ngarantias = $_GET['id'];
      
      if ($detalle->save())
      {
         $this->new_message('Detalle guardados correctamente.');
      }
      else
      {
         $this->new_error_msg('Imposible guardar el detalle.');
         return FALSE;
      }
   }
   
   
   //funcion copiada del SAT, todavia por editar
   public function nuevo_cliente() {
      //----------------------------------------------
      // agrega un cliente nuevo y retorna el id
      //----------------------------------------------

      if (isset($_POST['nombre'])) {
         $cliente = new cliente();
         $cliente->referencia = $cliente->get_new_codigo();
         $cliente->nombre = $_POST['nombre'];
         $cliente->nombrecomercial = $_POST['nombre'];
         $cliente->cifnif = $_POST['cifnif'];
         $cliente->telefono1 = $_POST['telefono1'];
         $cliente->telefono2 = $_POST['telefono2'];
         $cliente->codserie = $this->empresa->codserie;

         if ($cliente->save()) {
            $dircliente = new direccion_cliente();
            $dircliente->referencia = $cliente->referencia;
            $dircliente->codpais = $_POST['pais'];
            $dircliente->provincia = $_POST['provincia'];
            $dircliente->ciudad = $_POST['ciudad'];
            $dircliente->codpostal = $_POST['codpostal'];
            $dircliente->direccion = $_POST['direccion'];
            $dircliente->descripcion = 'Principal';
            if ($dircliente->save()) {
               $this->new_message('Cliente agregado correctamente.');
            } else
               $this->new_error_msg("¡Imposible guardar la dirección del cliente!");
         } else
            $this->new_error_msg('Error al agregar los datos del cliente.');
      }

      return $cliente->referencia;
   }
   
   public function agrega_garantias()
   {
         $this->registro_garantias->modelo = $_POST['modelo'];
         $this->registro_garantias->averia = $_POST['averia'];
         $this->registro_garantias->accesorios = $_POST['accesorios'];
         $this->registro_garantias->observaciones = $_POST['observaciones'];
         $this->registro_garantias->prioridad = $_POST['prioridad'];
         $this->registro_garantias->estado = $_POST['estado'];
         $this->registro_garantias->posicion = $_POST['posicion'];
         $this->registro_garantias->codcliente = $_POST['codcliente'];
         $this->registro_garantias->factventa = $_POST['factventa'];
         $this->registro_garantias->fechfactventa=NULL;
         if( isset($_POST['fechfactventa']) )
         {
             if($_POST['fechfactventa']!='')
                $this->registro_garantias->fechfactventa = date('d-m-Y', strtotime($_POST['fechfactventa']));
         }
         $this->registro_garantias->factcompra = $_POST['factcompra'];
         $this->registro_garantias->fechfactcompra=NULL;
         if( isset($_POST['fechfactcompra']) )
         {
             if($_POST['fechfactcompra']!='')
                $this->registro_garantias->fechfactcompra = date('d-m-Y', strtotime($_POST['fechfactcompra']));
         }
         $this->registro_garantias->codproveedor = $_POST['codproveedor'];
         $this->registro_garantias->numserie = $_POST['numserie'];
         $this->registro_garantias->numrma = $_POST['numrma'];
         

         if ($this->registro_garantias->save())
         {
            $this->new_message('Datos del Garantia guardados correctamente.');
            return $this->registro_garantias->ngarantias;
         }
         else
         {
            $this->new_error_msg('Imposible guardar los datos del Garantia.');
            return FALSE;
         }
       
   }
   
   public function edita_garantias($id)
   {
       //me he quedado por aqui
         $this->registro_garantias->ngarantias = $id;
         $this->registro_garantias->modelo = $_POST['modelo'];
         $this->registro_garantias->averia = $_POST['averia'];
         $this->registro_garantias->accesorios = $_POST['accesorios'];
         $this->registro_garantias->observaciones = $_POST['observaciones'];
         $this->registro_garantias->prioridad = $_POST['prioridad'];
         $this->registro_garantias->estado = $_POST['estado'];
         $this->registro_garantias->posicion = $_POST['posicion'];
         $this->registro_garantias->codcliente = $_POST['codcliente'];
         $this->registro_garantias->factventa = $_POST['factventa'];
         $this->registro_garantias->fechfactventa=NULL;
         if( isset($_POST['fechfactventa']) )
         {
             if($_POST['fechfactventa']!='')
                $this->registro_garantias->fechfactventa = date('d-m-Y', strtotime($_POST['fechfactventa']));
         }
         $this->registro_garantias->factcompra = $_POST['factcompra'];
         $this->registro_garantias->fechfactcompra=NULL;
         if( isset($_POST['fechfactcompra']) )
         {
             if($_POST['fechfactcompra']!='')
                $this->registro_garantias->fechfactcompra = date('d-m-Y', strtotime($_POST['fechfactcompra']));
         }
         $this->registro_garantias->codproveedor = $_POST['codproveedor'];
         $this->registro_garantias->numserie = $_POST['numserie'];
         $this->registro_garantias->numrma = $_POST['numrma'];
         
         

         if ($this->registro_garantias->save())
         {
            $this->new_message('Datos de la Garantia guardados correctamente.');
            return $this->registro_garantias->ngarantias;
         }
         else
         {
            $this->new_error_msg('Imposible guardar los datos de la Garantia.');
            return FALSE;
         }
       
   }
   
}
   