<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2014-2015  Francisco Javier Trujillo   javier.trujillo.jimenez@gmail.com
 * Copyright (C) 2014  Carlos Garcia Gomez         neorazorx@gmail.com
 * Copyright (C) 2015  Francesc Pineda Segarra         shawe.ewahs@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_model('agente.php');
require_model('cliente.php');
require_model('proveedor.php');
require_model('detalles_garantias.php');
require_model('pais.php');
require_model('registro_garantias.php');

class listado_garantias extends fs_controller
{
   public $agente;
   public $cliente;
   public $registro_garantias;
   public $detalles_garantias;
   public $busqueda;
   public $resultado;
   public $estado;
   public $proveedor;
   public $num_detalles;
   public $pais;
   public $urlplugin="plugins/garantias/";
   
   public function __construct()
   {
      parent::__construct(__CLASS__, 'Garantías', 'ventas', FALSE, TRUE);
      /// cualquier cosa que pongas aquí se ejecutará DESPUÉS de process()
   }

   /**
    * esta función se ejecuta si el usuario ha hecho login,
    * a efectos prácticos, este es el constructor
    */
   protected function private_core()
   {
      
     /// desactivamos la barra de botones
      $this->show_fs_toolbar = FALSE;
      $this->cliente = new cliente();
      $this->proveedor= new proveedor();
      $this->registro_garantias = new registro_garantias();
      $this->detalles_garantias = new detalles_garantias();
      $this->pais = new pais();

      $this->busqueda = array(
          'buscar' => '',
          'estado' => 'todos',
          'proveedor' => 'todos',
          'orden' => ''//este lo he dejado para cuando francesc implemente el orden
      );
      
      /* Búsqueda de cliente con JSON sin recargar página */
      if (isset($_REQUEST['buscar_cliente']))
      {
         /// desactivamos la plantilla HTML
         $this->template = FALSE;

         $json = array();
         foreach ($this->cliente->search($_REQUEST['buscar_cliente']) as $cli)
         {
            $json[] = array('value' => $cli->nombre, 'data' => $cli->referencia);
         }

         header('Content-Type: application/json');
         echo json_encode(array('query' => $_REQUEST['buscar_cliente'], 'suggestions' => $json));
      }
      /* No lo tengo claro */
      elseif( isset($_GET['delete']) )
      {
         $garantias = $this->registro_garantias->get($_GET['delete']);
         if($garantias)
         {
            if( $garantias->delete() )
            {
               $this->new_message('Registro eliminado correctamente.');
            }
            else
            {
               $this->new_error_msg('No se ha podido eliminar el registro.');
            }
         }
         else
         {
            $this->new_error_msg('Registro no encontrado.');
         }
         
         $this->template = "garantias";
         $this->resultado = $this->registro_garantias->all();
      }
      else
      {
         //$this->cargar_extensiones();
         
         if(isset($_POST['buscar']) || isset($_POST['estado']) || isset($_POST['codproveedor']))
         {
            
            if (isset($_POST['buscar'])) {
               $this->busqueda['buscar'] = $_POST['buscar'];
            }
            
            if (isset($_POST['estado'])) {
               $this->busqueda['estado'] = $_POST['estado'];
            }
            
            if (isset($_POST['codproveedor'])) {
               $this->busqueda['proveedor'] = $_POST['codproveedor'];
            }
            
            
            
            $this->resultado = $this->registro_garantias->search($this->busqueda['buscar'], $this->busqueda['estado'], $this->busqueda['proveedor']);
            $this->template = "garantias";
         }
         else
         {
            $this->resultado = $this->registro_garantias->all();
            $this->template = "garantias";
         }
         
         
      }
   }

   /**
    * Agrega un cliente nuevo y retorna su id
    */
   public function nuevo_cliente()
   {
      if (isset($_POST['nombre']))
      {
         $cliente = new cliente();
         $cliente->referencia = $cliente->get_new_codigo();
         $cliente->nombre = $_POST['nombre'];
         $cliente->nombrecomercial = $_POST['nombre'];
         $cliente->cifnif = $_POST['cifnif'];
         $cliente->telefono1 = $_POST['telefono1'];
         $cliente->telefono2 = $_POST['telefono2'];
         $cliente->codserie = $this->empresa->codserie;

         if ($cliente->save())
         {
            $dircliente = new direccion_cliente();
            $dircliente->referencia = $cliente->referencia;
            $dircliente->codpais = $_POST['pais'];
            $dircliente->provincia = $_POST['provincia'];
            $dircliente->ciudad = $_POST['ciudad'];
            $dircliente->codpostal = $_POST['codpostal'];
            $dircliente->direccion = $_POST['direccion'];
            $dircliente->descripcion = 'Principal';
            
            if ($dircliente->save())
            {
               $this->new_message('Cliente agregado correctamente.');
            }
            else
            {
               $this->new_error_msg("¡Imposible guardar la dirección del cliente!");
            }
         }
         else
         {
            $this->new_error_msg('Error al agregar los datos del cliente.');
         }
      }

      return $cliente->referencia;
   }

   /**
    * Agrega una garantía nueva
    */
   public function agrega_garantias()
   {
      if ($this->cliente)
      {
         $this->registro_garantias->referencia = $_GET['referencia'];
         $this->registro_garantias->modelo = $_POST['modelo'];

         if ($_POST['fcomienzo'] == '')
         {
            $this->registro_garantias->fcomienzo = NULL;
         }
         else
         {
            $this->registro_garantias->fcomienzo = $_POST['fcomienzo'];
         }

         if ($_POST['ffin'] == '')
         {
            $this->registro_garantias->ffin = NULL;
         }
         else
         {
            $this->registro_garantias->ffin = $_POST['ffin'];
         }

         $this->registro_garantias->averia = $_POST['averia'];
         $this->registro_garantias->accesorios = $_POST['accesorios'];
         $this->registro_garantias->observaciones = $_POST['observaciones'];
         $this->registro_garantias->prioridad = $_POST['prioridad'];

         if ($this->registro_garantias->save())
         {
            $this->new_message('Datos de la garantía guardados correctamente.');
            return $this->registro_garantias->ngarantias;
         }
         else
         {
            $this->new_error_msg('No se ha podido guardar los datos de la garantía.');
            return FALSE;
         }
      }
      else
      {
         $this->new_error_msg('Cliente no encontrado.');
         return FALSE;
      }
   }
   
   /**
    * Edita una garantía
    */
   public function edita_garantias()
   {
      $this->resultado = $this->registro_garantias->get($_GET['id']);
      
      if($this->resultado)
      {
         $this->agente = $this->user->get_agente();
      }
      
      if (isset($_POST['detalle']))
      {
         $this->agrega_detalle();
      }
      else
      {
         if ($this->resultado AND isset($_POST['modelo']))
         {
            $cliente = $this->cliente->get($this->resultado->referencia);
            if ($cliente AND isset($_POST['nombre']))
            {
               $cliente->nombre = $_POST['nombre'];
               $cliente->nombrecomercial = $_POST['nombre'];
               $cliente->telefono1 = $_POST['telefono1'];
               $cliente->telefono2 = $_POST['telefono2'];

               if ($cliente->save())
               {
                  $this->new_message('Cliente modificado correctamente.');
               }
               else
               {
                  $this->new_error_msg('Error al guardar los datos del cliente.');
               }
            }

            $this->resultado->modelo = $_POST['modelo'];
            $this->resultado->fcomienzo = $_POST['fcomienzo'];

            if ($_POST['ffin'] != '')
            {
               $this->resultado->ffin = $_POST['ffin'];
            }

            $this->resultado->averia = $_POST['averia'];
            $this->resultado->accesorios = $_POST['accesorios'];
            $this->resultado->observaciones = $_POST['observaciones'];
            $this->resultado->posicion = $_POST['posicion'];
            $this->resultado->prioridad = $_POST['prioridad'];
            
            //si tiene el mismo estado no tiene que hacer nada sino tiene que añadir un detalle
            if ($this->resultado->estado != $_POST['estado'])
            {
               $this->resultado->estado = $_POST['estado'];
               $this->agrega_detalle_estado($_POST['estado']);
            }
            
            if ($this->resultado->save())
            {
               $this->new_message('Datos del Garantia guardados correctamente.');
            }
            else
            {
               $this->new_error_msg('Imposible guardar los datos del Garantia.');
            }
         }
         else if (!$this->resultado)
         {
            $this->new_error_msg('Datos no encontrados.');
         }
      }
   }
   
   /**
    * Retorna una lista de estados con sus respectivos id
    * 
    * En registro_garantias::estados() nos devuelve un array con todos los estados,
    * pero como queremos también el id, pues hay que hacer este bucle para sacarlos.
    */
   public function listar_estados()
   {
      $estados = array();
      foreach ($this->registro_garantias->estados() as $i => $value)
      {
         $estados[] = array('id_estado' => $i, 'nombre_estado' => $value);
      }

      return $estados;
   }

   /**
    * Retorna una lista de prioridades con sus respectivos id
    * 
    * En registro_garantias::prioridad() nos devuelve un array con todos los prioridades,
    * pero como queremos también el id, pues hay que hacer este bucle para sacarlos.
    */
   public function listar_prioridad()
   {
      $prioridad = array();
      foreach ($this->registro_garantias->prioridad() as $i => $value)
      {
         $prioridad[] = array('id_prioridad' => $i, 'nombre_prioridad' => $value);
      }

      return $prioridad;
   }

   /**
    * Listar el detalle de garantías
    */
   public function listar_garantias_detalle()
   {
      return $this->detalles_garantias->all_from_garantias($_GET['id']);
   }

   /**
    * Agregar el detalle de garantía
    */
   public function agrega_detalle()
   {
      $detalle = new detalles_garantias();
      $detalle->descripcion = $_POST['detalle'];
      $detalle->ngarantias = $_GET['id'];
      
      if ($detalle->save())
      {
         $this->new_message('Detalle guardados correctamente.');
      }
      else
      {
         $this->new_error_msg('Imposible guardar el detalle.');
         return FALSE;
      }
   }

   /**
    * Agregar el detalle estado de garantía
    */
   public function agrega_detalle_estado($estado)
   {
      $detalle = new detalles_garantias();
      $detalle->descripcion = "Se a cambiado el estado a : " . $this->registro_garantias->nombre_estado_param($estado);
      $detalle->ngarantias = $_GET['id'];
      
      if ($detalle->save())
      {
         $this->new_message('Detalle guardados correctamente.');
      }
      else
      {
         $this->new_error_msg('Imposible guardar el detalle.');
         return FALSE;
      }
   }

   /**
    * Función para cargar las extensiones necesarias
    */
   private function cargar_extensiones()
   {
      /// añadimos la extensión para clientes
      $fsext0 = new fs_extension(
         array(
            'name' => 'cliente_garantias',
            'page_from' => __CLASS__,
            'page_to' => 'ventas_cliente',
            'type' => 'button',
            'text' => '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Garantías',
            'params' => ''
         )
      );
      if( !$fsext0->save() )
      {
         $this->new_error_msg('Imposible guardar los datos de la extensión.');
      }
   }
}
